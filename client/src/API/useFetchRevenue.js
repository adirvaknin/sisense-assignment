import income from './income.json'
import expence from './expence.json'
import dayjs from 'dayjs'
import isBetween from 'dayjs/plugin/isBetween'
import _ from 'lodash'
dayjs.extend(isBetween)


const useFetchRevenue = (startDate, endDate) => {
	const relevantIncome = income.filter(
		currentIncome => dayjs(currentIncome.date).isBetween(dayjs(startDate), dayjs(endDate))
	)
	const relevantExpences = expence.filter(
		currentExpence => dayjs(currentExpence.date).isBetween(dayjs(startDate), dayjs(endDate))
	)

	const incomeByDay = relevantIncome.reduce((acc, currentValue) => {
		return {
			...acc,
			[currentValue.date]: acc[currentValue.date] 
				? acc[currentValue.date] + currentValue.value
				: currentValue.value
		}
	}, {})
	const expenceByDay = relevantExpences.reduce((acc, currentValue) => {
		return {
			...acc,
			[currentValue.date]: acc[currentValue.date] 
				? acc[currentValue.date] + currentValue.value
				: currentValue.value
		}
	}, {})

	const allDates = _.uniq([...Object.keys(incomeByDay), ...Object.keys(expenceByDay)])

	const parsedChartData = allDates.map(date => [
		date, 
		incomeByDay[date], 
		expenceByDay[date], 
		incomeByDay[date] - expenceByDay[date]
	])

	return parsedChartData
}

export default useFetchRevenue