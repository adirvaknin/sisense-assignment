import React from 'react'
import HomePage from './homePage/HomePage'
import { DateFilterProvider } from './homePage/providers/DateFilterProvider'

const App = () => {
	return (
		<DateFilterProvider>
			<HomePage/>
		</DateFilterProvider>
	)
}

export default App
