import React, { useState } from 'react'
import TextField from '@mui/material/TextField'
import AdapterDayjs from '@mui/lab/AdapterDayjs'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import DatePicker from '@mui/lab/DatePicker'
import { Button, Grid } from '@mui/material'
import useDateFilterContext from './providers/DateFilterProvider'

const Header = () =>  {
	const { 
		startDate: contextStartDate, 
		endDate: contextEndDate,
		setStartAndEndDate
	} = useDateFilterContext()
	const [startDate, setStartDate] = useState(contextStartDate)

	const handleStartDateChange = (newValue) => {
		setStartDate(newValue)
	}

	const [endDate, setEndDate] = useState(contextEndDate)

	const handleEndDateChange = (newValue) => {
		setEndDate(newValue)
	}

	const handleFilterClick = () => {
		setStartAndEndDate(startDate, endDate)
	}

	return (
		<LocalizationProvider dateAdapter={AdapterDayjs}>
			<Grid item container direction={'row'} spacing={2} justifyContent={'center'}>
				<Grid item>
					<DatePicker
						renderInput={(props) => <TextField {...props} />}
						label="start date"
						value={startDate}
						onChange={handleStartDateChange}
						inputFormat="DD/MM/YYYY"
					/>
				</Grid>
				<Grid item>
					<DatePicker
						renderInput={(props) => <TextField {...props} />}
						label="end date"
						value={endDate}
						onChange={handleEndDateChange}
						inputFormat="DD/MM/YYYY"
					/>
				</Grid>
				<Grid item>
					<Button variant='outlined' sx={{ height: '100%' }} onClick={handleFilterClick}> 
						{'filter'} 
					</Button>
				</Grid>
			</Grid>
		</LocalizationProvider>
	)
}

export default Header