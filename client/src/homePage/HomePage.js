import React from 'react'
import useFecthRevenue from '../API/useFetchRevenue'
import IncomeGraph from './IncomeGraph'
import { Grid } from '@mui/material'
import Header from './Header'
import Footer from './Footer'
import useDateFilterContext from './providers/DateFilterProvider'

const HomePage = () => {
	const { startDate, endDate } = useDateFilterContext()
	const data = useFecthRevenue(startDate, endDate)
	return (
		<Grid container direction={'column'} justifyContent={'center'}>
			<Header />
			<Grid item>
				<IncomeGraph data={data}/>
			</Grid>
			<Grid item>
				<Footer />
			</Grid>
		</Grid>
	)
}

export default HomePage