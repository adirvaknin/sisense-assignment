import React from 'react'
import { Chart } from 'react-google-charts'
import PropTypes from 'prop-types'

const options = {
	title: 'Store revenue',
	curveType: 'function',
	legend: { position: 'bottom' },
	colors:['red', 'blue', 'green']
}

const IncomeGraph = (props) => {
	return (
		<Chart
			chartType="LineChart"
			width="100%"
			height="400px"
			data={[['date', 'Sales', 'Expenses', 'Revenue'], ...props.data]}
			options={options}
		/>
	)
}
IncomeGraph.propTypes = {
	data: PropTypes.array.isRequired
}

export default IncomeGraph
