import dayjs from 'dayjs'
import React, { useContext, useState } from 'react'
import { createContext } from 'react'
import PropTypes from 'prop-types'

const DateFilterContext = createContext()

export const DateFilterProvider = ({ children }) => {
	const [startDate, setStartDate] = useState(dayjs('01/06/2021', 'DD-MM-YYYY'))
	const [endDate, setEndDate] = useState(dayjs('31/12/2021', 'DD-MM-YYYY'))

	const setStartAndEndDate = (startDate, endDate) => {
		setStartDate(startDate)
		setEndDate(endDate)
	}
	return (
		<DateFilterContext.Provider value={{
			startDate,
			endDate,
			setStartAndEndDate
		}}>
			{children}
		</DateFilterContext.Provider>
	)
}
DateFilterProvider.propTypes = {
	children: PropTypes.element.isRequired
}

const useDateFilterContext = () => useContext(DateFilterContext)

export default useDateFilterContext
